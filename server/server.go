package net

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"

	"de/blockchainkids/govotee/blockchain"
	"de/blockchainkids/govotee/net"
)

var chain blockchain.Chain

func NewServer(ch *blockchain.Chain) {
	chain = *ch
}

func Run() error {
	handler := makeRouter()
	httpPort := os.Getenv("PORT")

	log.Println("starting server on ", httpPort)

	server := &http.Server{
		Addr:         ":" + httpPort,
		Handler:      handler,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 15 * time.Second,
	}

	if err := server.ListenAndServe(); err != nil {
		return err
	}

	return nil
}

func makeRouter() http.Handler {
	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/", handleGetBlockchain).Methods("GET")
	muxRouter.HandleFunc("/", handleWriteBlock).Methods("POST")
	return muxRouter
}

func handleGetBlockchain(wr http.ResponseWriter, r *http.Request) {
	data, err := json.MarshalIndent(chain, "", "  ")
	if err != nil {
		http.Error(wr, err.Error(), http.StatusInternalServerError)
		return
	}
	// TODO: save to server (io)
	println(wr, string(data))
}

func handleWriteBlock(wr http.ResponseWriter, r *http.Request) {
	var request net.Message
	decoder := json.NewDecoder(r.Body)

	err := decoder.Decode(&request)
	if err != nil {
		respondJson(wr, r, http.StatusBadRequest, r.Body)
		return
	}
	defer r.Body.Close()

	response := net.Message{Vote: request.Vote}

	//TODO: wrap in if IsValid

	prevBlock := chain.Blocks[len(chain.Blocks)-1]
	newBlock := blockchain.NewBlock(response.Vote, chain.Blocks[len(chain.Blocks)-1])

	if chain.IsBlockValid(*newBlock, *prevBlock) {
		chain.AddBlock(response.Vote)
		encoder := json.NewEncoder(wr)
		encoder.Encode(&response)
	}

	respondJson(wr, r, http.StatusCreated, newBlock)
}

func respondJson(wr http.ResponseWriter, r *http.Request, statusCode int, payload interface{}) {
	response, err := json.MarshalIndent(payload, "", "  ")
	if err != nil {
		wr.WriteHeader(http.StatusInternalServerError)
		wr.Write([]byte("HTTP 500: Internal Server Error"))
		return
	}

	wr.WriteHeader(statusCode)
	wr.Write(response)
}
