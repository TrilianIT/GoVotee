package main

import (
	"de/blockchainkids/govotee/blockchain"
	"fmt"
	"log"
	"sync"

	"github.com/joho/godotenv"

	serv "de/blockchainkids/govotee/server"
)

var mutex = &sync.Mutex{}

func main() {
	err := godotenv.Load()

	if err != nil {
		log.Fatal(err)
	}

	chain := blockchain.NewChain()

	chain.AddBlock("Tristan")
	chain.AddBlock("Tristan")

	for _, block := range chain.Blocks {
		fmt.Println("Id: ", block.Id)
		fmt.Println("Time: ", block.TimeStamp)
		fmt.Println("Data: ", block.Data)
		fmt.Println("Hash: ", block.Hash)
		fmt.Println("Previous Hash: ", block.PrevHash)
	}

	//p2p.Run()

	serv.NewServer(chain)
	log.Fatal(serv.Run())
}
